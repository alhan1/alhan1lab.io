---
layout: page
title: Nasim Ramezani
permalink: /nasimramezani/
---

<amp-img width="359" height="453" layout="responsive" src="{{ site.url }}{{ site.nasimramezani }}"></amp-img>

<p align="justify">
Nasim started her music career with classical vocal in 2003. She owes her music perception and singing knowledge and techniques mainly to Mehdi Ghasemi, Narbeh Cholakian and Hasmik Karapetian. 
After immigration, she began experiencing more different musical genres such as folk, world music and jazz. Currently she is the lead singer in an international music band called Nakuk.
</p>

<p align="justify">
Follow Nasim on Facebook: <a href="https://www.facebook.com/nasim.rmz.1/" style="text-decoration : none; color:brown;">Nasim Ramezani</a>
</p>
