---
layout: page
title: Nima Noury
permalink: /nimanoury/
---

<amp-img width="371" height="453" layout="responsive" src="{{ site.url }}{{ site.nimanoury }}"></amp-img>
<small>
	Photo: 
	<a href="https://www.margemellow.de" style="text-decoration : none; color:brown;">Maciej Staszkiewicz</a>
</small>

<p align="justify">
Nima Noury has been born in Tehran in 1983. He holds a PhD in Neuroscience and is a scientist at the university hospital of Tübingen, Germany. However, parallel to his scientific career, he has been continuously fulfilling his extreme passion for music by learning traditional Iranian music from some of the greatest masters of Iran, including Houshang Zarif, Hamid Motebassem, Behrooz Hemmati, Amir Hossein Reza, etc. He plays several Iranian instruments: Tar, SeTar, Oud, and Tar Bass. 
</p>

<p align="justify">
Since 2011, Nima is living in Germany and has been playing in several international and Iranian bands, including <a href="https://www.facebook.com/mianensemble/" style="text-decoration : none; color:brown;">Mian-Ensemble</a>, Folklang, Dilan, Shapol, etc. In September 2019, Nima founded Alhan Ensemble, which focuses on Iranian traditional music. 
</p>

<p align="justify">
Apart from his musical activities as a composer and performer, Nima teaches Tar and Setar, and also gives workshops on the theory of the middle-eastern music.
</p>

<i>
Follow Nima on Facebook: <a href="https://www.facebook.com/NimaNoury.Tar/" style="text-decoration : none; color:brown;">Nima Noury</a>
</i>
<br/><br/>

<!--
<p align="center"><b>Some sample works of Nima with other bands</b></p>
-->

<!--
<div class="iframe-container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/Gjjii0KBx-c" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<p align="center"> <i>
Mian Ensemble' arraignment of "Gypsy perfume", an Arabic tune, composed by Anouar Brahem
</i> </p>

<p align="center">
Mian Ensemble playing a novel arrangment of "Prelude to Isfahan", an old Iranian tune, composed by Morteza Neydavoud:
</p>
<div class="iframe-container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/OvH9QifwvA8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<p align="center">
Mian Ensemble playing "Panderinho", composed by Kathryn Döhner inspired by the folk music of Brazil:
</p>
<div class="iframe-container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/ziGjW-75Q-s" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

<p align="center"><b>Listen to some solo performances of Nima</b></p>

<br/><br/>
<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/468647232&color=%23492416&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>
<p align="center">
<i>
Longa Farahfaza, an Egyptian tune composed by Riad Al Sunbati. Arranged for and performed on Tar
</i>
</p>

<br/><br/>
<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/672945089&color=%23492416&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>
<p align="center">
<i>
Gilaki, a piece by Hamid Motebassem, performed on Oud
</i>
</p>

<br/><br/>
<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/468692853&color=%23492416&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>
<p align="center">
<i>
Reng in Shur mode, a very old Iranian tune composed by Ali Akbar Shahnazi, performed on Tar
</i>
</p>

<p align="center">
Reng in Chahargah mode, old Iranian tune composed by Rokneddin Mokhtari. Performed on Tar, together with Saba Hajinasab on Daf an Iranian Percussion (home recording):
</p>
<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/418404596&color=%23492416&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>

<p align="center">
Main Theme of Schindler's list, composed by John Williams. Performed on Se-Tar (home recording):
</p>
<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/676208603&color=%23492416&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>
-->