---
layout: page
title: Gallery
permalink: /gallery/
---

<br/><br/>
<div class="iframe-container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/GvDpQT1XRcE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<p align="center"> <i>
Oscar Antolí (Clarinet), Saeid Tehrani (Tonbak), and Nima Noury (Oud) playing a composition of Nima Noury
</i> </p>


<br/><br/>
<div class="iframe-container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/2cs33b2cT3A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<p align="center"> <i>
Nima Noury (Tar) and Saeid Tehrani (Tonbak) playing an old Iranian song Bayat-Tork Modus
</i> </p>


<br/><br/>
<div class="iframe-container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/0NIzCf1ZdL0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<p align="center"> <i>
Alhan Ensemble playing "Sorrowful Spring", a tune composed by Nima Noury
</i> </p>


<br/><br/>
<div class="iframe-container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/0DRBPogOwJ0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<p align="center"> <i>
Nima Noury playing a short program in Afshari Modus with Tar
</i> </p>


<br/><br/>
<div class="iframe-container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/SCnoQUeXyNA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<p align="center"> <i>
Nima Noury (Tar) and Saeid Tehrani (Tonbak) playing a composition of Nima Nouryin Chaargaah Modus
</i> </p>


<br/><br/>
<div class="iframe-container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/rqZYHOp9CAg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<p align="center"> <i>
Nima Noury playing his interpretation of an old Iranian song (the apple) with Oud
</i> </p>


<br/><br/>
<div class="iframe-container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/5Mt4ssG1UZU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<p align="center"> <i>
Nima Noury and Saba Hajinasab playing an Azeri Tune in Maqam Bayat Shiraz
</i> </p>


<br/><br/>
<div class="iframe-container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/G6I_8ymp0nk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<p align="center"> <i>
Nima Noury playing a short program in Esfahan Modus with Tar
</i> </p>


<br/><br/>
<div class="iframe-container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/2FtReIY8RMU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<p align="center"> <i>
Nima Noury playing "Nahoft" in Maqam Nava, a piece composed by Abolhasan Saba and later arranged by Parviz Meshkatian
</i> </p>


<br/><br/>
<div class="iframe-container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/NDCt7U2x_qg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<p align="center"> <i>
Nima Noury playing “For Cherries”, one of his compositions in Maqam Bayat Tork
</i> </p>


<br/><br/>
<div class="iframe-container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/251BEtaWxjE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<p align="center"> <i>
Nima Noury playing a Taqsim in Maqam Abouatā
</i> </p>


<br/><br/>
<div class="iframe-container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/zE0OMGqf0qM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<p align="center"> <i>
Nima Noury playing “Those Days”, one of his compositions in Maqam Esfehan
</i> </p>


<br/><br/>
<div class="iframe-container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/oNSSXw67lOI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<p align="center"> <i>
Nima Noury playing a folk song from north Iran 
</i> </p>


<br/><br/>
<div class="iframe-container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/Gjjii0KBx-c" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<p align="center"> <i>
Nima plays "Gypsy perfume" with Mian Ensemble, an Arabic tune, composed by Anouar Brahem
</i> </p>

<br/><br/>
<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/468647232&color=%23492416&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>
<p align="center">
<i>
Longa Farahfaza, an Egyptian tune composed by Riad Al Sunbati. Arranged for and performed on Tar by Nima Noury
</i>
</p>

<br/><br/>
<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/672945089&color=%23492416&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>
<p align="center">
<i>
Gilaki, a piece by Hamid Motebassem, performed on Oud by Nima Noury
</i>
</p>

