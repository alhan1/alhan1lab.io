---
layout: page
title: About
permalink: /about/
---
<!--
<amp-img width="600" height="400" layout="responsive" src="{{ site.url }}{{ site.cover }}"></amp-img>
-->
<p align="justify"><i>
Alhan is the term that was used to refer to "tunes" or “melodies” in all middle eastern countries before the 20th century. 
</i></p>

<p align="justify"><i>
Alhan Ensemble was founded in Sep. 2019 by Nima Noury. We play traditional Iranian music. Our repertoire mainly consists of tunes composed by Nima. However, we also enjoy making new arrangements of old traditional Iranian songs.
</i></p>

<p align="justify"><i>
Apart from giving concerts, we are also happy to give workshops on Iranian music. 
</i></p>

<b>
Alhan Ensemble: 
</b>

<a href="{{ site.url }}/nimanoury/" style="text-decoration : none; color:brown;">Nima Noury</a>: Tar, Oud and Vocal 

<a href="https://www.oscarantoli.com/" style="text-decoration : none; color:brown;">Oscar Antolí</a>: Clarinet 

<a href="https://www.facebook.com/tehrani.saeid/" style="text-decoration : none; color:brown;">Saeid Tehrani</a>: Tonbak 

<a href="{{ site.url }}/nasimramezani/" style="text-decoration : none; color:brown;">Nasim Ramezani</a>: Vocal 

<!--
<i>Contact: AlhanTunes@gmail.com </i>

For upcoming events, check our Facebook page: <a href="https://www.facebook.com/alhantunes/" style="text-decoration : none; color:brown;">Alhan Ensemble</a>
<br/><br/>
<div class="iframe-container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/Gjjii0KBx-c" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
-->
